---
layout: post
title:  Some closure properties of amenable groups
date:   2024-01-08 00:01:00 +0000
category: algebra
description: Collecting some results about the class of discrete amenable groups.
---
Here is a small collection of properties that amenable groups enjoy.

**Definition**: A discrete group $$G$$ is called amenable, if there exists a function $$\mu\colon 2^G \to [0,1]$$ such that

1. $$\mu(G) = 1$$.
2. For all $$A,B \subseteq G$$ with $$A \cap B = \varnothing$$ we have $$\mu(A \cup B) = \mu(A) + \mu(B)$$.
3. For all $$A \subseteq G$$ and $$g \in G$$ we have $$\mu(Ag) = \mu(A)$$.

Hence, $$\mu$$ is a finitely additive right-invariant probability measure on $$G$$. We call such a function $$\mu$$ an invariant mean of $$G$$.

**Remark**: Since $$\mu$$ is not $$\sigma$$-additive, we cannot directly apply the results of measure theory here. So first we have to convince ourselves, that we can integrate at least bounded functions with respect to invariant means.

**Definition**: A simple function on $$G$$ is a function $$f\colon G \to \bR$$ which is a linear combination of indicator functions

$$ f(x) = \sum_{i=1}^n \lambda_i \mathbf{1}_{A_i}(x) $$

where $$n \in \bN, ~ \lambda_i \in \bR, ~ A_i \subseteq G$$. The simple functions form a vector space $$\mathcal{E}(G)$$.

**Remark**: From measure theory we know that simple functions have a standard form in which the indicators are of disjoint sets. Furthermore, every simple function has a finite range and is therefore bounded. The integral on simple functions can be defined exactly as for $$\sigma$$-additive measures.

**Proposition**: Let $$(B(G), \norm{\cdot}_\infty)$$ be the Banach space of bounded functions on $$G$$ equipped with the sup norm. The subspace $$\mathcal{E}(G) \leq B(G)$$ is dense.

**Proof**: Let $$f\colon G \to \bR$$ be a bounded function. Suppose $$f$$ is positive. Then $$f(G) \subseteq [0, b)$$ for some $$b > 0$$. Partition $$[0, b)$$ into $$n$$ subintervals $$[a_i, b_i)$$. Then the $$A_i \coloneqq f^{-1}([a_i, b_i))$$ form a partition of $$G$$. Now the function $$g = \sum_{i=1}^n a_i \mathbf{1}_{A_i} \in \mathcal{E}(G)$$ satisfies $$\norm{f - g}_\infty \leq \eps$$ where $$\eps > 0$$ is the mesh size of the partition. For the general case, split $$f = f^+ - f^-$$ and apply the same argument to $$f^+$$ and $$f^-$$. By refining the partition we can make the mesh size as small as needed, hence $$\mathcal{E}(G)$$ is dense in $$B(G)$$.

**Corollary**: The integral of bounded functions with respect to invariant means is well-defined.

**Proof**: Let $$\mu$$ be an invariant mean of $$G$$. Notice that integration with respect to $$\mu$$ defines a linear map

$$ \mathcal{E}(G) \to \bR, \quad f \mapsto \int_G f \ d\mu $$

which is Lipschitz. Indeed, we have

$$ \abs{\int_G f \ d\mu} \leq \int_G \abs{f} \ d \mu \leq \norm{f}_\infty \int_G \mathbf{1}_G \ d\mu = \norm{f}_\infty. $$

Since $$\mathcal{E}(G)$$ is dense in $$B(G)$$, we obtain a unique continuous extension $$B(G) \to \bR$$. This defines the integral of bounded functions.

**Theorem**: Every subgroup of an amenable group is amenable.

**Proof**: Let $$G$$ be amenable group with invariant mean $$\mu$$ and $$H \leq G$$. Choose a left transversal $$L \subseteq G$$ of $$H$$ and define

$$ \nu\colon 2^H \to [0, 1], \quad A \mapsto \mu(LA). $$

Then $$\nu$$ has all the required properties. In particular $$\nu(H) = \mu(LH) = \mu(G) = 1$$.

**Theorem**: Every quotient of an amenable group is amenable.

**Proof**: Let $$\mu$$ be an invariant mean of $$G$$ and let $$N \trianglelefteq G$$ with projection $$\pi\colon G \to G/N$$. Then the pushforward measure

$$\nu\colon 2^{G/N} \to [0,1], \quad A \mapsto \mu(\pi^{-1}(A))$$

has the required properties.

**Theorem**: Let $$1 \to N \to G \to G/N \to 1$$ be an extension. Then $$G$$ is amenable if and only if both $$N$$ and $$G/N$$ are amenable.

**Proof**: Due to the previous results it suffices to show that if $$N$$ and $$G/N$$ are amenable, then so is $$G$$. Let $$\mu$$ and $$\nu$$ be invariants of $$N$$ and $$G/N$$ respectively. For each $$A \subseteq G$$ define

$$ f_A\colon G \to [0, 1], \quad g \mapsto \mu(Ag^{-1} \cap N). $$

Notice that $$f_A$$ is constant on each coset of $$N$$. Indeed, if $$x = ny$$ for some $$n \in N$$ then

$$f_A(x) = \mu(Ax^{-1} \cap N)  = \mu((Ay^{-1} \cap N)n^{-1}) = \mu(Ay^{-1} \cap N) = f_A(y). $$

Hence, the $$f_A$$ descend to the quotient $$\bar f_A\colon G/N \to [0, 1]$$. Since bounded functions are integrable, we can define an invariant mean for $$G$$ via

$$m\colon 2^G \to [0, 1], \quad A \mapsto \int_{G/N} \bar f_A \ d\nu. $$

This map has the desired properties due to the following facts.
1. $$f_G = \mu(G (\cdot)^{-1} \cap N) = \mu(N) = 1$$.
2. For disjoint $$A,B \subseteq G$$ we have $$f_{A \cup B} = f_A + f_B$$.
3. For $$A \subseteq G$$ and $$g \in G$$ we have $$f_{Ag}(x) = f_A(xg^{-1})$$.
4. $$\nu$$ is right-invariant and thus $$\int_{G/N} \bar f_{A}(Nxg^{-1}) \ d\nu(Nx) = \int_{G/N} \bar f_{A}(Nx) \ d\nu(Nx)$$

**Corollary**: $$G$$ and $$H$$ are amenable groups if and only if $$G \times H$$ is amenable.

**Proof**: Observe that $$1 \to G \to G \times H \to H \to 1$$ is an extension.

**Theorem**: Directed colimits of amenable groups are amenable.

**Proof**: Let $$(G_i)_{i \in I}$$ be a directed system of amenable groups with invariant means $$(m_i)_{i \in I}$$ and let $$G = \colim G_i$$. Using the cocone maps $$f_i\colon G_i \to G$$ we extend the $$m_i$$ to

$$ \mu_i\colon 2^G \to [0, 1], \quad A \mapsto m_i(f_i^{-1}(A)). $$

To define the invariant mean of $$G$$ we use an appropriate ultrafilter limit. For $$g \in G$$ define

$$ I_g = \{ i \in I : \mu_i(Ag) = \mu_i(A) \text{ for all } A \subseteq G \}. $$

The family $$\{ I_g : g \in G \} \subseteq 2^I$$ has the finite intersection property. Indeed, let $$g_1, \dots, g_n \in G$$. Each $$g_k$$ has a preimage under some $$f_{i_k}\colon G_k \to G$$. Since $$I$$ is directed, the set $$\{ i_1, \dots, i_n \} \subseteq I$$ has an upper bound $$s \in I$$ which satisfies $$f_{i_k} = f_s \circ f_{i_k, s}$$ so that $$\mu_s$$ is invariant under the right action of all $$g_k$$. We conclude $$s \in \bigcap_{k=1}^n I_{g_k} \neq \varnothing$$.

We can therefore extend the family $$\{ I_g : g \in G \}$$ to an ultrafilter $$\mathcal U$$ on $$I$$ and define

$$\mu\colon 2^G \to [0, 1], \quad A \mapsto \lim_{i \to \mathcal U} \mu_i(A).$$

The ultralimit inherits finite additivity and $$\mu(G) = 1$$ from the $$\mu_i$$ and since all $$I_g \in \mathcal U$$, we see that $$\mu$$ is right invariant under the action of $$G$$.

**Corollary**: All abelian groups are amenable.

**Proof**: Let $$G$$ be an abelian group. Then $$G$$ is the directed colimit of its finitely generated subgroups $$(G_i)_{i \in I}$$ ordered by inclusion. Each $$G_i$$ is of the form $$\bZ^n \times \bZ/q_1\bZ \times \dots \times \bZ/q_r\bZ$$. By the previous results $$G_i$$ is amenable if $$\bZ$$ is amenable. To construct the invariant mean on $$\bZ$$, let $$F_n \coloneqq [-n, n] \cap \bZ$$ and define

$$ \mu_n \colon 2^{\bZ} \to [ 0, 1 ], \quad A \mapsto \frac{\abs{F_n \cap A}}{\abs{F_n}}. $$

It is easy to see that each $$\mu_n$$ is a finitely additive probability measure. Furthermore, we have

$$
\begin{align*}
\mu_n(A \pm 1) &= \frac{\abs{F_n \cap (A \pm 1)}}{\abs{F_n}}
\leq \frac{\abs{(F_n \mp 1) \cap A}}{\abs{F_n}} \\
&\leq \frac{\abs{F_n \cap A}}{\abs{F_n}} + \frac{\abs{((F_n \mp 1) \setminus F_n) \cap A}}{\abs{F_n}} \\
&\leq \mu_n(A) + \frac{1}{2n+1}
\end{align*}$$

We fix a non-principal ultrafilter $$\mathcal U$$ on $$\bN$$ and define

$$\mu\colon 2^\bZ \to [0, 1], \quad A \mapsto \lim_{n \to \mathcal U} \mu_n(A). $$

Then $$\abs{\mu_n(A) - \mu_n(A \pm 1)} \to 0$$ shows that $$\mu$$ is invariant under the group action.