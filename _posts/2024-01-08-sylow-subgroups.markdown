---
layout: post
title:  "Sylow subgroups made easy"
date:   2024-01-08 00:00:00 +0000
category: algebra
description: The fastest way to show the existence of Sylow subgroups
---
Here I want to present a quick and easy proof of the first Sylow theorem. It uses basic combinatorics only and yields a super elementary proof of Cauchy's theorem for free. The goal is to prove the following statement:

**Theorem**: Let $$G$$ be a finite group of order $$\abs{G} = p^r m$$ where $$p$$ is a prime number and $$m$$ is coprime to $$p$$. Then $$G$$ has a subgroup of order $$p^r$$.

We need a small number-theoretic lemma which has a beautiful proof using group actions of cyclic groups.

**Lemma**: Let $$p$$ be a prime number and $$m \in \bN_+$$. Then for all $$r \in \bN$$ we have

$$ \binom{p^r m}{p^r} \equiv m \mod p.$$

**Proof**: Let $$C$$ be the cyclic group of order $$p^r$$. This group acts on itself by left multiplication, so the action extends to $$S \coloneqq \bigsqcup_{i=1}^m C$$. Hence, we have the desired action on 

$$X = \{ A \subseteq S : \abs{A} = p^r \}.$$

Notice that $$\abs{X} = \binom{p^r m}{p^r}$$. Let $$ A \in X$$. Then we have $$\abs{\Orb(A)} \cdot \abs{\Stab(A)} = p^r$$ and therefore

$$ \abs{X} \equiv \abs{\operatorname{Fix}_C(X)} \mod p. $$

Now, $$A \in X$$ is fixed by $$C$$ precisely if $$A$$ is a union of copies of $$C$$. But since $$\abs{A} = \abs{C}$$, it is exactly a single copy of $$C$$. Hence, there are $$m$$ fixed points. We conclude $$\abs{X} \equiv m \mod p$$.

With this in mind we can now prove the existence of Sylow subgroups.

**Proof (of theorem)**: Let $$G$$ act on $$X \coloneqq \{ A \subseteq G : \abs{A} = p^r \}$$ by left multiplication. Since $$m$$ and $$p$$ are coprime, we conclude $$p \nmid \abs{X}$$ by the previous lemma. Hence, there exists some $$A \in X$$ with $$p \nmid \abs{\Orb(A)}$$ and thus $$\abs{\Stab(A)} \geq p^r$$. Finally, for each $$a \in A$$, the map

$$\Stab(A) \to A, \quad x \mapsto xa$$

is injective, showing $$\abs{\Stab(A)} \leq p^r$$. We conclude that $$\Stab(A)$$ is the desired subgroup of order $$p^r$$. Neat.

A very nice consequence of this argument is that we get Cauchy's theorem practically for free.

**Corollary** (Cauchy's theorem): Let $$G$$ be a finite group. If a prime number $$p$$ divides $$\abs{G}$$, then $$G$$ has an element of order $$p$$.

**Proof**: By the previous theorem, $$G$$ has a subgroup $$P$$ of order $$p^r$$ for some $$r > 0$$. Let $$x \in P \setminus \{ 1 \}$$. Then by Lagrange's theorem, $$x$$ has order $$p^k$$ for some $$0 < k \leq r$$. Hence, the element 

$$y \coloneqq x^{p^{k-1}}$$ 

has order $$p$$, since $$y \neq 1$$ and $$y^p = x^{p^k} = 1$$. 