---
layout: post
title:  "Algebra golf: Determinants"
date:   2023-07-29 00:00:00 +0000
category: algebra
description: Let's find the quickest way to show that the general linear group induces an affine group scheme!
---

It is time for some recreational linear algebra. Todays' challenge will be to show that $$\GL_n$$ is an affine group scheme in the most economical way possible (even if it takes away some algebraic insight.) 

We apply the following completely arbitrary set of rules:

1. Using basic results from linear algebra in vector spaces is allowed, but we have no definition of determinants yet.
2. Avoid explicit combinatorics whenever possible.
2. Assuming knowledge of tensor products and basic category theory is allowed. 

If $$R$$ is a commutative ring, we define $$\GL_n(R)$$ to be the group of invertible $$n \times n$$ matrices. If we can define determinants in some "clean" way and show that

$$ \GL_n(R) = \{ A \in M_n(R) : \det(A) \in R^\times \}, $$

we are essentially done.

# A painless definition of determinants

Let $$ R $$ be a commutative unital ring. For any $$ R $$-module $$ M $$ we define the $$n$$-th exterior power of $$M$$ as the module

$$ \begin{gather*}
\Lambda^n(M) \coloneqq M^{\otimes n} / J \\
\text{where } J = \vspan \{ m_1 \otimes \dots \otimes m_n \in M^{\otimes n} : m_i = m_j \text{ for some } i \neq j \}. \end{gather*} $$

We write $$ m_1 \wedge \dots \wedge m_n $$ for the image of $$ m_1 \otimes \dots \otimes m_n $$ by the quotient map $$ M^{\otimes n} \to \Lambda^n(M) $$. 

If $$ f\colon M \to N $$ is a linear map, then $$ f^{\otimes n} $$ clearly descends to a linear map $$ \Lambda^n(M) \to \Lambda^n(N) $$. Since the tensor product is functorial, so is this assignment. We denote the functor by $$ \Lambda^n\colon R\text{-Mod} \to R\text{-Mod} $$.

Now suppose $$ M $$ is free of rank $$ n $$. Then $$ \Lambda^n(M) $$ is free of rank $$ 1 $$. To see this, let $$ \{ e_1, \dots, e_n \} $$ be a basis of $$ M $$. We claim that $$s = e_1 \wedge e_2 \wedge \dots \wedge e_n $$ generates $$ \Lambda^n(M) $$. Any nonzero $$n$$-wedge of the $$ \{ e_i \} $$ must contain each basis element exactly once so we can identify them with a permutation in $$ S_n $$. Observe that

$$ e_1 \wedge e_2 + e_2 \wedge e_1 = (e_1 + e_2) \wedge (e_1 + e_2) = 0 $$

so if $$ \sigma = \tau \circ \pi $$ for a transposition $$ \tau $$, then the corresponding wedges satisfy $$ e_\sigma = - e_\pi $$. Since the transpositions generate $$ S_n $$ we conclude

$$ e_\sigma = \operatorname{sgn}(\sigma) e_{\id} = \operatorname{sgn}(\sigma) s $$

and $$ s $$ is a free generator of $$ \Lambda^n(M) $$.

Let $$ f\colon M \to M $$ be a linear map and suppose $$ M $$ is again free of rank $$ n $$. Since $$ \End_R(\Lambda^n(M)) \iso R $$, there exists a unique element $$ \det(f) \in R $$ such that

$$ \Lambda^n(f) = \det (f) \cdot \id. $$

# Properties for free

Since $$ \Lambda^n $$ is functorial, we immediately obtain the properties
1. $$ \det(\id_M) \cdot \id = \Lambda^n(\id_M) = 1 \cdot \id $$
2. $$ \det(g \circ f) \cdot \id = \Lambda^n(g \circ f) = \Lambda^n(g) \circ \Lambda^n(f) = \det(g) \cdot \det(f) \cdot \id $$
3. $$ f \in \Aut_R(M) \implies \Lambda^n(f) \in \Aut_R(\Lambda^n(M)). $$

In particular, if $$f$$ is invertible, then so is $$\det(f)$$. We are already halfway there.

# Matrices

For the remainder of the discussion we shall work with matrices. Consider the free module $$ R^n $$ with the standard basis $$ \{ e_1, \dots, e_n \} $$. We identify a matrix $$ A \in M_n(R) $$ with the linear map $$x \mapsto Ax$$ and a linear map $$ f\colon R^n \to R^n $$ with the matrix $$(a_{ij})$$ whose entries are determined by the equations

$$ f(e_i) = \sum_j a_{ij} e_j. $$

Suppose $$A$$ has columns $$v_1, \dots, v_n$$. Note that 

$$\Lambda^n(A)(e_1 \wedge \dots \wedge e_n) = v_1 \wedge \dots \wedge v_n = \det(A) \wedge e_1 \wedge \dots \wedge e_n. $$

This shows that determinants are linear in each column of the matrix $$A$$. Writing the $$\{ v_i \}$$ as linear combinations of the standard basis, we see that $$\det(A)$$ is given by a polynomial in the entries $$\{ a_{ij} \}$$ with integer coefficients (recall that wedges of the standard basis are integer linear combinations of $$s$$ as we have seen before).

# Cramer's rule

Let $$A \in M_n(R)$$ and let $$A_i(x)$$ be the matrix obtained by replacing column $$i$$ in $$A$$ by $$x \in R^n$$. Since determinants are linear in each column, the map

$$f\colon R^n \to R^n, \quad x \mapsto \begin{bmatrix} \det A_1(x) & \det A_2(x) & \dots & \det A_n(x) \end{bmatrix}^\top $$

is linear. We notice that $$f(Ae_i) = \det(A) e_i$$ so identification of $$f$$ with a matrix $$B$$ leads to

$$BA = \det(A) I.$$ 

To see that $$AB = \det(A) I$$ also holds, we use a neat shortcut. The columns of $$B$$ are given by

$$ \begin{bmatrix} \det A_1(e_i) & \dots & \det A_n(e_i) \end{bmatrix}^\top $$ 

so if we replace $$A$$ by an indeterminate matrix $$X = (x_{ij})$$, the entries of $$B$$ are integer coefficient polynomials in the $$(x_{ij})$$ and thus $$XB - \det(X)I$$ consists of $$n^2$$ polynomials in $$\bZ[x_{ij} : 1 \leq i,j \leq n]$$. All those polynomials vanish on $$M_n(\bC)$$ because left and right inverses coincide for complex matrices by the rank-nullity theorem. Hence, all the polynomials are zero. Since for any $$A \in M_n(R)$$ we obtain $$AB - \det(A)I$$ by evaluating those $$n^2$$ polynomials in the entries of $$A$$, we have $$AB = \det(A)I$$ for all $$A \in M_n(R)$$.

We conclude that if $$\det A \in R^\times$$, then $$A$$ is invertible and thus 

$$\GL_n(R) = \{ A \in M_n(R) : \det(A) \in R^\times \}.$$

Remark: Our proof of Cramer's rule shows in some opaque way that $$M_n(R)$$ is Dedekind-finite -- a property that we could have derived from Nakayama's lemma as well.

# Finishing the challenge

Fix a commutative ring $$k$$. Mapping a $$k$$-algebra morphism $$f\colon R \to S$$ to 

$$ \GL_n(f) \colon \GL_n(R) \to \GL_n(S), \quad \quad (a_{ij}) \mapsto (f(a_{ij})) $$

is functorial because matrix multiplication and determinants are polynomial in the matrix entries. Hence, we have a functor $$\GL_n\colon \operatorname{CAlg}_k \to \operatorname{Grp}$$. 

Let $$X = (x_{ij})$$ denote an indeterminate matrix and define the algebra

$$A = k[X, y] / (\det(X)y - 1). $$

It is clear that $$\operatorname{CAlg}_k(A, R)$$ can be identified with $$\GL_n(R)$$ by evaluation in $$X$$, hence

$$ \eta_R\colon \operatorname{CAlg}_k(A, R) \to \GL_n(R), \quad \quad f \mapsto (f(x_{ij})) $$

define a natural isomorphism $$\eta\colon \operatorname{CAlg}_k(A, -) \Rightarrow \GL_n$$ and $$\GL_n$$ is representable, making it an affine group scheme.

# Bonus: Eigenvalue theory

So far we have not derived an explicit formula for the determinant. As it turns out, we can do surprisingly much without a formula. Let us develop some eigenvalue theory.

In this chapter we take $$k$$ to be a field and $$V$$ is a $$k$$-vector space of dimension $$n \in \bN$$.

## Characteristic polynomials

Let $$f\colon V \to V$$ be a linear map. The characteristic polynomial of $$f$$ is defined as

$$ p_f = \det(f - t \id_V) \in k[t]. $$

This is well-defined because functoriality of $$\Lambda^n$$ implies that $$\det$$ is invariant under change of basis and for any fixed basis we know that $$\det$$ is polynomial in the coefficients that determine the linear operator.

## Eigenvalues as roots

Let $$f\colon V \to V$$ be a linear map. A scalar $$\lambda \in k$$ is an eigenvalue of $$f$$, if the linear system

$$ f(v) = \lambda v$$

has a non-trivial solution $$v \in V \setminus \{0 \}$$. In that case, we call $$v$$ an eigenvector of $$f$$.

We claim that the eigenvalues of $$f$$ are precisely the roots of $$p_f \in k[t]$$. Indeed, if $$f(v) = \lambda v$$ has no solutions $$v \neq 0$$, then $$f - \lambda \id$$ is an isomorphism and thus $$p_f(\lambda) \in k^\times$$. On ther other hand, if some $$v \neq 0$$ satisfies the equation, then we can extend $$v = v_1$$ to a basis $$\{ v_1, \dots, v_n \}$$ of $$V$$ and $$g = f - \lambda \id$$ satisfies

$$ 0 = g(v_1) \wedge \dots \wedge g(v_n) = \det(g) \cdot v_1 \wedge \dots \wedge v_n. $$

We conclude $$p_f(\lambda) = 0$$.

## Extracting coefficients

We can now establish that $$\det(f)$$ is the product of the eigenvalues in the algebraic closure of $$k$$. To see this, we wish to find the degree and leading coefficient of $$p_f$$ without using an explicit formula for the determinant. Consider the case $$k = \bC$$. Then $$p_f$$ is continuous as a polynomial function and we have

$$ \frac{p_f(z)}{z^n} = \frac{\det(f - z \id)}{z^n} = \det(f/z - \id) \xrightarrow{|z| \to \infty} \det(- \id) = (-1)^n $$ 

and clearly for higher powers of $$z$$ this becomes zero. Hence $$\deg(p_f) = n$$ with leading coefficient $$(-1)^n $$. Since this holds for all $$f$$, the equation must also hold for the generic integer polynomial that, evaluated in the coefficients of $$f$$, determines $$p_f$$. Therefore, this fact about the characteristic polynomial holds in any field.

Let $$k$$ now be any field. In the algebraic closure of $$k$$ we can factorize

$$p_f = (-1)^n \prod_{i=1}^n (t - \lambda_i) $$

and thus we have

$$ \det(f) = p_f(0) = (-1)^n \prod_{i=1}^n (- \lambda_i) = \prod_{i=1}^n \lambda_i. $$

# Conclusion

It is surprising how much information can be obtained about determinants without knowing some explicit formula in terms of matrix coefficients. Of course, this is not a good way to go about it because such proofs leave out most of the algebraic insight. It is still an interesting recreational challenge and perhaps there is an even more economical way to solve it!