---
layout: post
title:  "Kan extensions diagrammatically"
date:   2024-01-30 00:00:00 +0000
category: category theory
description: The right way to understand concepts in 2-categories?
---
Today I want to present an alternative way for reasoning about (strict) 2-categories that allows us to express seemingly difficult concepts in a very simple way visually. To see how powerful this approach is, we will study (weak) Kan extensions as a concept in the 2-category of (adequately size bounded) categories. 

# Introducing string diagrams

The key insight here is that in this context we mainly care about natural transformations. Functors serve as endpoints for the natural transformations and categories are just ambient and not important for our arguments. Unfortunately, the usual commutative diagram notation realize natural transformations as 2-dimensional objects (faces). To shift the focus onto natural transformations instead, we dualize commutative diagrams.

![Poincaré duality](/assets/images/kan-extensions/poincare-duality.png)

This dual notation is a string diagram. There are different conventions for how to write and read them. The notation I will use here reflects contravariant composition in formulas exactly. This allows us to translate the diagram into a formula by reading it top to bottom and left to right. Furthermore:

- Functor composition $$G \circ F$$ is realized by drawing two parallel strings.
- Identity functors are omitted.
- The identity natural transformations are omitted.

Using our conventions we can introduce the notation for composing natural transformations. 

![Composition](/assets/images/kan-extensions/composition.png)

The true power of the string diagram calculus in proofs comes from the fact that certain equations turn into topological properties of the diagrams that we can easily use to manipulate the equations visually. The interchange law for horizontal and vertical composition of natural transformations tells us that we can freely slide the "marbles" on the strings up and down as long as they retain their relative order.

![Interchange law](/assets/images/kan-extensions/interchange-law.png)

Notice how easy it is to express whiskering in this notation. It is practically automatic!

# Adjunctions

Given functors $$L\colon \mathcal{C} \to \mathcal{D}$$ and $$R\colon \mathcal{D} \to \mathcal{C}$$, an adjunction $$L \dashv R$$ is realized by two natural transformations

$$ \eta\colon \text{Id}_\mathcal{C} \Longrightarrow RL \quad \text{and} \quad \eps\colon LR \Longrightarrow \text{Id}_{\mathcal{D}}$$

that satisfy the zig-zag law (or equivalently the triangle identities). We shall write them out directly in string diagram notation. The corresponding formulas are easily read off from the images.

![Adjunction laws](/assets/images/kan-extensions/adjunction.png)

Since we omit the string of the identity functors, the unit $$\eta$$ and counit $$\eps$$ are realized as a cup and cap respectively. The zig-zag identities are topologically intuitive from the diagrams. They will play an important role in understanding how adjunctions interact with different constructions.

# Kan extensions

A left Kan extension of a functor $$F\colon \mathcal{C} \to \mathcal{E}$$ with respect to a functor $$K\colon \mathcal{C} \to \mathcal{D}$$ is given by a functor $$\operatorname{Lan}_K F\colon \mathcal{D} \to \mathcal{E}$$ and a natural transformation $$\alpha\colon F \Longrightarrow (\operatorname{Lan}_K F) \circ K$$ with the following universal property.

![Left kan extension](/assets/images/kan-extensions/lan-def.png)

Visually, each $$\beta$$ splits into $$\alpha$$ and some unique $$\delta$$. Sliding $$\delta$$ into $$\alpha$$ creates a unique $$\beta$$. 

Dualizing this construction, the definition of a right Kan extension is obtained by flipping source and target of the involved natural transformations. In string diagram notation this corresponds to mirroring the picture vertically!

![Right kan extension](/assets/images/kan-extensions/ran-def.png)

# Everything is a Kan extension?

As it turns out, almost everything can be written as a Kan extension.

## Adjunctions are Kan extensions

Suppose we have an adjunction $$L \dashv R$$ with unit $$\eta\colon \text{Id}_{\mathcal{C}} \Longrightarrow RL$$ and counit $$\eps\colon LR \Longrightarrow \text{Id}_{\mathcal{D}}$$. We will see the following.

1. $$(R, \eta)$$ is a left Kan extension of $$\text{Id}_{\mathcal{C}}$$ with respect to $$L$$.
2. $$(L, \eps)$$ is a right Kan extension of $$\text{Id}_{\mathcal{D}}$$ with respect to $$R$$.

Using duality, it suffices to show only the first claim. (Notice how the right-adjoint is the left Kan extension and vice versa.)

Let $$\beta\colon \text{Id}_{\mathcal{C}} \Longrightarrow GL$$ be given. If any $$\delta\colon R \Longrightarrow G$$ factors $$\beta$$, then it must satisfy the following equations.

![Adjunctions are Kan extensions - proof](/assets/images/kan-extensions/adjunctions-kan-ext-proof.png)

But this determines $$\delta$$ uniquely as $$G\eps \circ \beta R$$. According to the zig-zag identity, this $$\delta$$ clearly works and thus the proof is complete!

## Limits and colimits are Kan extensions

To see that limits and colimits can be identified with certain Kan extensions, we simply notice that a cone to a functor $$F$$ is exactly a natural transformation from the constant functor that picks the apex of the cone to the functor $$F$$. 

![Cones as natural transformations](/assets/images/kan-extensions/cone-factorization.png)

Since every constant functor factors uniquely through the terminal category, we have a decomposition that fits the framework of Kan extensions. It is now obvious that the terminal cone $$\lim F$$ corresponds exactly to the right Kan extension of $$F$$ with respect to $$C \to \mathbf{1}$$.

Dually, the initial cocone $$\colim F$$ corresponds exactly to the left Kan extension of $$F$$ with respect to $$C \to \mathbf{1}$$.

# Adjoints preserve Kan extensions

Suppose we have a an adjunction $$L \dashv R$$.
1. If $$(\operatorname{Lan}_K F, \alpha)$$ is a left Kan extension and $$LF$$ composes, then $$(L \operatorname{Lan}_K F, L \alpha)$$ is a left Kan extension of $$LF$$ with respect to $$K$$.
2. If $$(\operatorname{Ran}_K F, \alpha)$$ is a right Kan extension and $$RF$$ composes, then $$(R \operatorname{Ran}_K F, R \alpha)$$ is a right Kan extension of $$RF$$ with respect to $$K$$.

By duality it suffices to prove only the first claim. Using the string diagram calculus, the proof basically writes itself.

Suppose we are given $$\beta\colon LF \Longrightarrow GK$$ and it factors through $$L \alpha$$ via $$\delta\colon L (\operatorname{Lan}_K) F \Longrightarrow G$$. Adding some zig-zag identities we come to the following conclusion.

![Adjoints preserve Kan extensions proof](/assets/images/kan-extensions/adjoints-preserve-kan.png)

Where $$\hat \delta$$ is the unique natural transformation that factors $$\gamma = \beta \circ \eta F$$ through $$\alpha$$. It is again clear that this $$\delta$$ factors $$\beta$$, so this concludes the proof.

**Corollary:** Left adjoints preserve colimits and right adjoints preserve limits.

# Wrapping up

Many more interesting things can be said about Kan extensions and we can prove the properties using string diagrams. For example, if we try to take a Kan extension of a functor with respect to itself, we obtain certain (co)density monads. Furthermore, we can use Kan extensions to understand / define derived functors in convenient generality. 

It should be noted that the string diagram calculus works extremely well in the context of monoidal categories (effectively 2-categories with a single object.) Perhaps an idea for another post...