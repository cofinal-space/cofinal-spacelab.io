---
layout: post
title:  "Monads and Adjunctions"
date:   2024-04-21 00:00:00 +0000
category: category theory
description: Exploring the Eilenberg-Moore adjunction with string diagrams.
---
Welcome to another episode of categories by diagrams! Today we will investigate the connection between adjunctions and monads. The results I present here are quite elementary. However, it is a nice exercise for learning how to utilize string diagrams. Of course, to the trained category theorist this approach will be less efficient than directly applying Yoneda-type arguments. Still, I think the string diagrams let us easily understand the underlying structure (compared to the usual commutative diagrams.)

Note: All diagrams use the notation from my [post about Kan extensions]({% post_url 2024-01-30-kan-extensions %}).

## Monads
Let $$\mathcal{C}$$ be category. The functor category $$([\mathcal{C}, \mathcal{C}], \circ, \mathrm{Id})$$ is (strict) monoidal, and so we may define monoid objects in it. A monad is a monoid object in this category. More explicitly, a monad is a functor $$T\colon \mathcal{C} \to \mathcal{C}$$ together with a multiplication and unit

$$\mu\colon T \circ T \Longrightarrow T, \quad \eta\colon \mathrm{Id} \Longrightarrow T$$

such that the monoid axioms are satisfied. The associativity and unit axioms are readily expressed in string diagram notation.

![Monoid laws](/assets/images/monads-and-adjunctions/monoid-laws.png)

Notice that by horizontally flipping the string diagrams we obtain the definition of a comonad which has to satisfy the coassociativity and counit axioms.

## Monads from Adjunctions
It turns out that every adjunction gives rise to both a monad and a comonad in a natural way. In fact, this is obvious from the string diagrams!

Suppose we have an adjunction $$L \dashv R$$ with unit $$\eta\colon \text{Id} \Rightarrow R \circ L$$ and counit $$\eps\colon L \circ R \Rightarrow \text{Id}$$. Then $$\eta$$ is already the unit of the corresponding monad where $$T = R \circ L$$. The multiplication is given by whiskering the counit in the obvious way. The associativity and unit laws are a direct consequence of the zig-zag identities and naturality.

![Monad from adjunction](/assets/images/monads-and-adjunctions/monad-adjunction.png)

By flipping the diagrams horizontally and exchanging the roles of $$\eta$$ and $$\eps$$ we obtain a comonad with underlying functor $$C = L \circ R$$.

## Adjunctions from monads?
We understand that every adjunction yields a monad. But does every monad come from an adjunction? More precisely: Given a monad $$(T, \mu, \eta)$$, can we construct an adjunction $$L \dashv R$$ whose monad is $$(T, \mu, \eta)$$? The answer to that question is "yes" and we can do it in more than one way. One such construction is the Eilenberg-Moore adjunction which effectively reconstructs the monad from its category of modules.

## Modules over a monad
Traditionally a module over a monoid is an object on which the monoid acts. We generalize this notion to monads: A module over a monad $$T\colon \mathcal{C} \to \mathcal{C}$$ is an object $$x \in \mathcal{C}$$ with a $$T$$-action $$a\colon Tx \to x$$ in $$\mathcal{C}$$ that satisfies the usual axioms we would require of a module action. To express the laws in string diagram notation, we identify objects $$x \in \mathcal{C}$$ with the functor $$x\colon \mathbf{1} \to \mathcal{C}$$ that selects the object. A morphism $$x \to y$$ in $$\mathcal{C}$$ corresponds to a natural transformation $$x \Rightarrow y$$ between those functors. Here is the resulting string diagram for the action, along with the two module axioms (1) and (2).

![Monad module laws](/assets/images/monads-and-adjunctions/monad-module-laws.png)

Let $$x,y$$ be two $$T$$-modules with actions $$a\colon Tx \to x$$ and $$b\colon Ty \to y$$. We say that $$f\colon x \to y$$ is a morphism of $$T$$-modules, if it is compatible with the actions, i.e. the following diagram equation must hold.

![Module morphism](/assets/images/monads-and-adjunctions/module-morphism.png)

It's obvious from the diagrams that $$T$$-modules form a category. This category is called the Eilenberg-Moore category and usually denoted by $$\mathcal{C}^T$$.

## Free Modules
To reconstruct $$T$$ we need to define a functor $$F\colon \mathcal{C} \to \mathcal{C}^T$$ which assigns each object in $$\mathcal{C}$$ a "free" $$T$$-module. Since we have no information apart from $$\mu$$ and $$\eta$$, the only reasonable thing to do is to use $$\mu_x\colon T(Tx) \to Tx$$ as an action on $$Tx$$ which doesn't really depend on any information about $$x$$. Indeed, it is completely obvious from the diagrams that $$\mu_x$$ is an action on $$Tx$$ for any $$x \in \mathcal{C}$$ because it doesn't interact with $$x$$ at all.

![Regular T-module](/assets/images/monads-and-adjunctions/regular-t-module.png)

Similarly, we can now observe that any morphism $$f\colon x \to y$$ in $$\mathcal{C}$$ is lifted by $$T$$ to a morphism of $$T$$-modules. This is simply due to naturality of $$\mu$$.

![Regular T-module](/assets/images/monads-and-adjunctions/free-functor.png)

Hence, we can define the functor $$F$$ via

$$F\colon \mathcal{C} \to \mathcal{C}^T, \quad x \mapsto (Tx, \mu_x), \quad (f\colon x \to y) \mapsto (Tf\colon Tx \to Ty).$$

## The Reconstruction

It is left to show that $$F \dashv U$$ where $$U\colon \mathcal{C}^T \to \mathcal{C}$$ simply forgets the action. We want the unit $$\eta$$ of $$T$$ to be the unit of the adjunction. The components of the counit $$\eps\colon F \circ U \Rightarrow \mathrm{Id}$$ have the form $$Ty \to y$$ so we define them to be the $$T$$-action of each $$y \in \mathcal{C}^T$$. The naturality condition of $$\eps$$ is merely stating that all morphisms in $$\mathcal{C}^T$$ are $$T$$-equivariant which holds by definition. The zig-zag identities are easy to work out using string diagrams.

![EM-adjunction](/assets/images/monads-and-adjunctions/em-adjunction.png)

## Outlook

We have seen one particular way to build an adjunction for a given monad. Another way to build an adjunction from a monad is by using the associated Kleisli category. By turning the adjunctions that produce a fixed monad into a category, one can show that the Eilenberg-Moore construction is terminal and the Kleisli construction is initial in this setting. In fact, one can show that those two constructions are equivalent when all modules over the monad are free.