---
layout: default
title: About
permalink: /about/
---

Handcrafted with <i class="fa-solid fa-heart" style="color: #f66151;"></i> and of course no JavaScript. Maybe one day this will correctly render in emacs.